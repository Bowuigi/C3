#include <stdio.h>
#include "../c3.h"

// 3 simple functions to use C3 as a config file format along with an example
// License: 0BSD

unsigned int Cfg_Find(C3_Node *n, char *key) {
	if (n == NULL) return 0;

	for (unsigned int i = 1;; i++, n = n->next) {
		if (!strcmp(key, n->name)) {
			return i;
		};

		if (n->next == NULL) {
			return 0;
		}
	}
}

C3_Node *Cfg_Index(C3_Node *n, unsigned int index) {
	for (unsigned int i = 1; i < index; i++, n = n->next);

	return n;
}

C3_Node *Cfg_Get(C3_Node *n, char *key) {
	unsigned int i = Cfg_Find(n, key);

	if (i == 0) return NULL;

	return Cfg_Index(n, i);
}

int main() {
	FILE *f = fopen("config.c3", "r");

	if (!f) {
		fputs("Unable to open config.c3", stderr);
		return 1;
	}

	C3_Node *n = C3_ParseFile(f);

	if (C3_Error != C3_OK) {
		fputs("Failed to parse file", stderr);
		return 1;
	}

	C3_WriteTree(stdout, n);

	C3_Node *first = n->child;
	C3_Node *category = Cfg_Get(first, "Category")->child;
	C3_Node *opt1 = Cfg_Get(category, "Opt1");
	C3_Node *opt7 = Cfg_Get(category, "Opt7");

	printf("Find /Category: %s\n", (category ? "Yes" : "No"));
	printf("Find /Category/Opt1: %s", (opt1 ? "Yes" : "No"));

	if (opt1) printf(" = %s", opt1->value);

	printf("\nFind /Category/Opt7: %s", (opt7 ? "Yes" : "No"));

	if (opt7) printf(" = %s", opt7->value);

	putchar('\n');

	C3_DestroyNode(n, C3_DESTROY_NORMAL, C3_DESTROY_NORMAL);

	fclose(f);
}
