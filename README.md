# C3

A C rewrite for Ulfnic's BAAM (Bash Associative Arrays in Multi-dimensions), useful for anything that requires trees (like a configuration format)

C3 is used in [Unit21](https://codeberg.org/Bowuigi/Unit21) as the official config file format because of its readability and simple syntax

Example:

```
\ This is a comment
									\ Comments can have any level of indentation

This is a node
	this is its child, which is also a node
	nodes can have both childs
	and: values!

You can have more than one base node
	and as you saw,
	node names can contain most characters!
	the only forbidden ones are tabs (at the beginning),
	colons before the value and newlines
	and \\ as the first character

The way those work
	is very simple,
	it is a binary tree with
	2 extra values for data!

One node has four fields:
	name : string
	value : string
	next : node*
	child : node*
```

C3 has a few differences from BAAM:

- Nodes are allowed to lack a value (which allows for easy arrays)
- Elements are treated as a binary tree instead of as arrays
- A writer is included
- No use of Regex
- Faster
- Comments are allowed

# Including instructions and API

To use it, just drop the c3.c, c3.h and LICENSE files on your project (on a separate folder, for example), then, add `#include "c3.h"` and add c3.c to the files to compile

The API is as follows:

```c
C3_Node *C3_CreateNode(char *name, char *value);
```

Create a node with the given name and value

```c
void C3_DestroyNode(C3_Node *node, int delete_next, int delete_child);
```

Destroy the given node, optionally destroying the whole tree

```c
void C3_WriteTree(FILE *out, C3_Node *node);
```

Write the given node (and its children) to the given file

```c
C3_Node *C3_ParseFile(FILE *in);
```

Parse a file and return the node structure (the nodes are children of a top node which is returned)

---

The relevant fields on the node are its next and child pointers, which can be used to traverse the node tree recursively.

Those can be also used to create your own node trees from code or modify existing trees (to change settings based on user selections, for example)

Examples of using it as a config file format and more are in the **examples** folder
