/*
 * C3: A C rewrite (with modifications and optimizations)
 * of BAAM (Bash Associative Arrays in Multi-dimensions)
 * that is used as a generic config and description file
 * format in Unit21
 *
 * Licensed under the 0BSD license, like the original project
 *
 * Thanks to Ulfnic for creating the original implementation
*/

// Header guard
#ifndef C3_H
#define C3_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// Length of parser strings, counts NUL
#ifndef C3_STR_LEN
#define C3_STR_LEN 512
#endif

// Max depth of a tree
#ifndef C3_MAX_DEPTH
#define C3_MAX_DEPTH 64
#endif

// Counts whitespace and ':'
#ifndef C3_LINE_LEN
#define C3_LINE_LEN (C3_STR_LEN * 2) + 3 + C3_MAX_DEPTH
#endif

// Helpers to call the destroy function
#define C3_DESTROY_NORMAL 0
#define C3_DESTROY_NEXT 1
#define C3_DESTROY_CHILD 1

// Posible errors for C3 functions
// Like errno but just for C3
enum C3_Errors {
	C3_OK,
	C3_ERR_INDENT,
	C3_ERR_OOM,
	C3_ERR_NAME_TOO_LONG,
	C3_ERR_VALUE_TOO_LONG,
	C3_ERR_MAX_DEPTH
};

extern enum C3_Errors C3_Error;

/* A node is composed of a simple structure:
 * node
 * 	name
 * 	value (optional, defaults to NULL)
 * 	child (pointer to the first child to simplify the implementation)
 * 		...
 * 	next (pointer to the next node on the same level)
 * 		...
*/
typedef struct C3_Node {
	char name[C3_STR_LEN];
	char value[C3_STR_LEN];

	struct C3_Node *child;
	struct C3_Node *next;
} C3_Node;

// Prototypes of public functions
// Look on the C file for commented declarations
C3_Node *C3_CreateNode(char *name, char *value);

void C3_DestroyNode(C3_Node *node, int delete_next, int delete_child);

void C3_WriteTree(FILE *out, C3_Node *node);

C3_Node *C3_ParseFile(FILE *in);

#endif // C3_H header guard
