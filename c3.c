/*
 * C3: A C rewrite (with modifications and optimizations)
 * of BAAM (Bash Associative Arrays in Multi-dimensions)
 * that is used as a generic config and description file
 * format in Unit21
 *
 * Licensed under the 0BSD license, like the original project
 *
 * Thanks to Ulfnic for creating the original implementation
*/

#include "c3.h"

enum C3_Errors C3_Error = C3_OK;

// Create a node, remember to destroy it with C3_DestroyNode after you are done using it
C3_Node *C3_CreateNode(char *name, char *value) {
	// Reset error
	C3_Error = C3_OK;

	// Allocate and zero the new node
	C3_Node *node = calloc(1, sizeof(C3_Node));

	// Handle out of memory condition
	if (!node) {
		C3_Error = C3_ERR_OOM;
		return NULL;
	}

	// Avoid memory weirdness by duplicating the name
	char *tmpn = strdup(name);

	// Copy the name to the new node
	strncpy(node->name, tmpn, C3_STR_LEN-1);
	// Ensure that the string is NULL terminated
	node->value[C3_STR_LEN-1] = '\0';

	// Free the temporal variable for correctness
	free(tmpn);

	// Do the same but for the value, but only if one is available
	if (value != NULL) {
		char *tmpv = strdup(value);
		strncpy(node->value, tmpv, C3_STR_LEN-1);
		node->value[C3_STR_LEN-1] = '\0';
		free(tmpv);
	}

	return node;
}

// Delete nodes recursively, optionally deleting the next one too (for freeing trees)
void C3_DestroyNode(C3_Node *node, int delete_next, int delete_child) {
	// Recurse downwards if delete_child is nonzero
	if (node->child != NULL && delete_child) {
		C3_DestroyNode(node->child, delete_next, delete_child);
	}

	// Recurse to next nodes if delete_next is nonzero
	if (node->next != NULL && delete_next) {
		C3_DestroyNode(node->next, delete_next, delete_child);
	}

	// Actually destroy the current node
	free(node);
}

// Write the tree to a file, use C3_WriteTree instead
static void C3__WriteTree(FILE *out, C3_Node *node, size_t depth) {
	// Show depth with tabs
	for (size_t i = 1; i < depth; i++) {
		putc('\t', out);
	}

	// Print information about the current node
	fprintf(out, "%s", node->name);

	// Print the value if available
	if (*node->value != '\0') {
		fprintf(out, ": %s", node->value);
	}

	putc('\n', out);

	// Recurse downwards
	if (node->child != NULL) {
		C3__WriteTree(out, node->child, depth+1);
	}

	// Recurse to next nodes
	if (node->next != NULL) {
		C3__WriteTree(out, node->next, depth);
	}
}

// Wrapper to the lower level static function that omits the depth argument
void C3_WriteTree(FILE *out, C3_Node *node) {
	C3__WriteTree(out, node, 1);
}

C3_Node *C3_ParseFile(FILE *in) {
	// Clean error
	C3_Error = C3_OK;

	// Define a few variables
	C3_Node *top = C3_CreateNode("Top node", NULL);
	char *line = calloc(C3_LINE_LEN, sizeof(char));
	int prev_depth = 0;
	C3_Node *prev = top;

	// Previous branches and a variable to index them
	C3_Node *prev_branches[C3_MAX_DEPTH] = {0};
	unsigned int prv_bn = 0;
	prev_branches[0] = top;

	// Out of memory
	if (!line) {
		C3_Error = C3_ERR_OOM;
		return NULL;
	}

	// Get a line from the file
	while (fgets(line, C3_LINE_LEN, in)) {
		// Skip empty lines
		if (line[0] == '\n' || line[0] == '\0') continue;

		// Check the indentation
		int depth;

		for (depth = 0; line[depth] == '\t'; depth++);
		depth++;

		if (line[depth-1] == '\\') continue;

		// Corroborate its validity
		// Indentation can't be bigger than current depth + 1
		if (depth > prev_depth + 1) {
			C3_Error = C3_ERR_INDENT;
			return NULL;
		}

		// Indentation can't be bigger than the maximum length
		if (depth > C3_MAX_DEPTH) {
			C3_Error = C3_ERR_MAX_DEPTH;
			return NULL;
		}

		// Create a node and set the fields key and value
		char tmpname[C3_STR_LEN] = {0};
		char tmpval[C3_STR_LEN] = {0};
		size_t nlen;
		size_t vlen;
		size_t off;
		int found_eol = 0;

		// Get the name of the node
		for (nlen = depth-1; line[nlen] != ':'; nlen++) {
			// If no value is supplied then this condition is triggered
			if (line[nlen] == '\0' || line[nlen] == '\n') {
				found_eol = 1;
				break;
			}

			// Handle errors
			if (nlen-(depth-1) > C3_STR_LEN-1) {
				C3_Error = C3_ERR_NAME_TOO_LONG;
				return NULL;
			}
			tmpname[nlen-(depth-1)] = line[nlen];
		}

		// Remove trailing whitespace
		for (nlen--; tmpname[nlen-(depth-1)] == ' ' || tmpname[nlen-(depth-1)] == '\t' || tmpname[nlen-(depth-1)] == '\n'; nlen--) tmpname[nlen-(depth-1)] = '\0';
		nlen++;

		// Store data directly if there is no value
		if (!found_eol) {
			// Skip past ':'
			off = nlen;
			while (line[off] == ' ' || line[off] == '\t' || line[off] == ':') off++;

			// Skip to letter
			for (; line[off] == ' ' || line[off] == '\t'; off++);

			// Get the value of the node
			for (vlen = off; line[vlen] != '\n' && line[vlen] != '\0'; vlen++) {
				if (vlen-off > C3_STR_LEN-1) {
					C3_Error = C3_ERR_VALUE_TOO_LONG;
					return NULL;
				}
				tmpval[vlen-off] = line[vlen];
			}

			// Delete trailing whitespace
			for (; tmpval[vlen-off] == ' '
				|| tmpval[vlen-off] == '\t'
				|| tmpval[vlen-off] == '\n'; vlen--) tmpval[vlen-off] = '\0';
		}

		// Store the node information
		C3_Node *current = C3_CreateNode(tmpname, tmpval);

		// If indentation it is the same as the previous line place the node on the "next" pointer
		if (depth == prev_depth) {
			prev->next = current;
		}

		// If it is bigger by one place it as a subtree and store the previous node's pointer
		// in the "previous branches" array
		if (depth > prev_depth) {
			prev->child = current;
			prev_branches[prv_bn] = prev;
			prv_bn++;
		}

		// If it is smaller then grab the corresponding pointer from the "previous branches" array
		if (depth < prev_depth) {
			prev_branches[depth]->next = current;
			prev_branches[depth] = current;
		}

		// Update the previous nodes and strings
		prev_depth = depth;
		prev = current;
	}

	// Clean up
	free(line);

	return top;
}
